import 'jspsych'
import 'jspsych/plugins/jspsych-html-keyboard-response'

import instructions from './assets/instructions/instructions.html'

export default function get_instructions() {
    return {
        type: 'html-keyboard-response',
        stimulus: instructions
    }
};