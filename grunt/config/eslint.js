module.exports = {
    target: ['./src/**/*.js', './src/**/*.jsx', './grunt/**/*.js'],
    options: {
        fix: true,
    },
};
